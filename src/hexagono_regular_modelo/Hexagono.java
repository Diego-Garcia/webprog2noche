/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hexagono_regular_modelo;

/**
 *
 * @author Lenovo
 */
public class Hexagono 
{
    public double lado1;
    public double lado2;
    public double lado3;
    public double lado4;
    public double lado5;
    public double lado6;

    public Hexagono(double lado1, double lado2, double lado3, double lado4, double lado5, double lado6) {
        this.lado1 = lado1;
        this.lado2 = lado2;
        this.lado3 = lado3;
        this.lado4 = lado4;
        this.lado5 = lado5;
        this.lado6 = lado6;
    }

    public double getLado1() {
        return lado1;
    }

    public void setLado1(double lado1) {
        this.lado1 = lado1;
    }

    public double getLado2() {
        return lado2;
    }

    public void setLado2(double lado2) {
        this.lado2 = lado2;
    }

    public double getLado3() {
        return lado3;
    }

    public void setLado3(double lado3) {
        this.lado3 = lado3;
    }

    public double getLado4() {
        return lado4;
    }

    public void setLado4(double lado4) {
        this.lado4 = lado4;
    }

    public double getLado5() {
        return lado5;
    }

    public void setLado5(double lado5) {
        this.lado5 = lado5;
    }

    public double getLado6() {
        return lado6;
    }

    public void setLado6(double lado6) {
        this.lado6 = lado6;
    }


        
}